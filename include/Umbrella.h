//do not give the Umbrella class normalized coords to store. It wants world coords.
//The fixture positions are relative to the centroid (local coords in world scale)

#pragma once

#include "cinder/gl/gl.h"
#include "cinder/Utilities.h"
#include "PixelCoordinate.h"

#define UMBRELLA_X_SIZE 0.3f
#define UMBRELLA_Y_SIZE 0.4f

using namespace ci;
//using namespace ci::app;

class Umbrella {
public:

	Umbrella(Vec2f upperLeft, std::vector<Point>& pixelCoordinates, Vec2f canvasSize);
	Umbrella();

	Vec2f getCentroid();
	
	Vec2f getFixtureWorldPosition(int index);
	void debugDraw();

private:
	Vec2f centroid;
	void spoofFixturePositions();
	std::vector<Vec2f> fixturePositions;

	float radians(float degrees);
};