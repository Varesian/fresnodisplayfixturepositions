#include "Umbrella.h"

Umbrella::Umbrella() {
}

Umbrella::Umbrella(Vec2f centroid_, std::vector<Point>& pixelCoordinates_, Vec2f canvasSize_) {
	//centroids are 0 through 1.0 coming from Light Act
	centroid = Vec2f(centroid_.x, centroid_.y);

	centroid.x -= 0.5f;
	centroid.y -= 0.5f;

	centroid.x *= canvasSize_.x;
	centroid.y *= canvasSize_.y;

	//centroid = Vec2f(upperLeft.x + 0, upperLeft.y + 0);

	spoofFixturePositions();
}

void Umbrella::spoofFixturePositions() {
	float barSpacing = 8.0f;

	float panelAngleOffset = 360 / 6;

	int fixtureCounter = 0;
	for (int panelNum = 0; panelNum < 6; panelNum++) {
		float angleOffset = panelNum * panelAngleOffset;
		float centroidOffset = panelNum * barSpacing;

		for (int barNum = 1; barNum <= 5; barNum++) {
			float ledAngleDiff = 60 / barNum; //how many degrees are between each led fixture
			int numLEDs = barNum;
			for (int ledNum = 1; ledNum <= numLEDs; ledNum++) {
				float theta = radians(angleOffset + -30.0f + ledAngleDiff * ledNum);
				float r = barSpacing * barNum;
				float x = r * cos(theta) + centroid.x;
				float y = r * sin(theta) + centroid.y;
				fixturePositions.push_back(Vec2f(x, y));
			}
		}
	}
}

void Umbrella::debugDraw() {
	gl::color(0, 1.0f, 1.0f, 1.0f);
	for (int i = 0; i < fixturePositions.size(); i++) {
		gl::drawSolidCircle(fixturePositions[i], 3);
	}
	gl::color(1.0f, 0, 0, 1.0f);
	gl::drawSolidCircle(centroid, 3);
}

float Umbrella::radians(float degrees) {
	return (degrees * M_PI) / 180.0f;
}

Vec2f Umbrella::getCentroid() {
	return centroid;
}